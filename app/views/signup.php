<!-- Signup Portion Start -->

<div class="main_tab_signup">
   <h1>Sign Up Here</h1><br>
    <form action="" method="POST">
        <fieldset class="form-group">

            <div class="col-xs-6">
                <label for="formGroupExampleInput">First Name</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Firstname" name="Fname" data-validation="length alphanumeric" data-validation-length="min4" data-validation-help="Input Your First Name">
           </div>

            <div class="col-xs-6">
                <label for="formGroupExampleInput">Last Name</label>
                <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Lastname" name="Lname" data-validation-help="Input Your Last Name">
            </div><br>

              <div class="col-xs-10 box-10">
                <label for="formGroupExampleInput">Username</label>
                <input type="text" class="form-control" id="formGroupExampleInput3" placeholder="Username" name="Uname" data-validation="alphanumeric" data-validation-allowing="-_" data-validation-help="Username will be atleast 6 charecters & you can use _ or -">
            </div><br>

             <div class="col-xs-10 box-10">
                <label for="formGroupExampleInput">Password</label>
                <input type="password" class="form-control" id="formGroupExampleInput4" placeholder="Password" name="pass_confirmation" data-validation="alphanumeric" data-validation-allowing="-_$" data-validation="length" data-validation-length="min8" data-validation="strength" data-validation-strength="2" data-validation-help="Password will be atleast 8 charecters & you can use _ or -">
           </div><br>

            <div class="col-xs-10 box-10">
               <label for="formGroupExampleInput">Confirm Password</label>
                <input type="password" class="form-control" id="formGroupExampleInput5" placeholder="Re-type Password" name="pass" data-validation="confirmation">
            </div><br>

             <div class="col-xs-6">
                <label for="formGroupExampleInput">Email</label>
                <input type="email" class="form-control" id="formGroupExampleInput6" placeholder="Email" name="email" data-validation="email" data-validation-help="Input your Valid email">
            </div>

            <div class="col-xs-6">
                <label for="formGroupExampleInput">Phone Number</label>
                <input type="phone" class="form-control" id="formGroupExampleInput7" placeholder="Phone Number" name="pNum" data-validation="number" data-validation-help="Input your Valid Phone Number"><br>
            </div><br>

             <div class="col-xs-10 box-10">
                <button class="w3-btn w3-blue w3-hover-light-blue" id="btn" name="submit">Sign Up</button><br>
           </div><br>

        </fieldset>
    </form>

    <a href="http://www.freebuck.xyz/polo/"><button class="w3-btn w3-blue w3-hover-light-blue">Homepage</button></a>

</div><br>
<!-- Signup Portion end -->

<script type="text/javascript">
$.validate({
   lang: 'en'
  });
$.validate({
    modules : 'security',
    onModulesLoaded : function() {
    var optionalConfig = {
      fontSize: '12pt',
      padding: '6px',
      bad : 'Very Weak',
      weak : 'Weak',
      good : 'Good',
      strong : 'Strong'
    };

    $('input[name="pass_confirmation"]').displayPasswordStrength(optionalConfig);
  }
  });
</script>
