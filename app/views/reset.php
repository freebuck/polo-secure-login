<!-- Reset Portion Start -->

<div class="main_tab_login">
	<h1>Reset Password</h1><br>
	<form action="" method="POST">
		<fieldset class="form-group">

			<div class="col-xs-10 box-10">
            <label for="formGroupExampleInput">Password</label>
            <input type="password" class="form-control" id="formGroupExampleInput4" placeholder="Password" name="pass_confirmation" data-validation="alphanumeric" data-validation-allowing="-_$" data-validation="length" data-validation-length="min8" data-validation="strength" data-validation-strength="2" data-validation-help="Password will be atleast 8 charecters & you can use _ or -">
        	</div><br>

	        <div class="col-xs-10 box-10">
	            <label for="formGroupExampleInput">Confirm Password</label>
	            <input type="password" class="form-control" id="formGroupExampleInput5" placeholder="Re-type Password" name="pass" data-validation="confirmation">
	        </div><br>

			<button class="w3-btn w3-blue w3-hover-light-blue" id="btn" name="submit" disabled="disabled">Change Password</button><br>
      
		</fieldset>
	</form>
</div><br>

<!-- Reset Portion end -->
<script type="text/javascript">
 $("#formGroupExampleInput4").keyup(function(){
    $("#formGroupExampleInput5").keyup(function(){
      console.log($(this).val().length);
        if($(this).val().length)
            $("#btn").prop('disabled', false);
        else
            $("#btn").prop('disabled', true);
    });
});
$.validate({
    lang: 'en'
  });
$.validate({
    modules : 'security',
    onModulesLoaded : function() {
    var optionalConfig = {
      fontSize: '12pt',
      padding: '6px',
      bad : 'Very Weak',
      weak : 'Weak',
      good : 'Good',
      strong : 'Strong'
    };

    $('input[name="pass_confirmation"]').displayPasswordStrength(optionalConfig);
  }
  });
</script>