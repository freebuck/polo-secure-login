<!-- Login Portion Start -->

<div class="main_tab_login">

  <h1>Sign In Here</h1><br>
  <form action="" method="POST">

    <fieldset class="form-group">

      <label for="formGroupExampleInput">Username</label>
      <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Username" name="name" data-validation-help="Username here" >


      <label for="formGroupExampleInput2">Password</label>
      <input type="password" class="form-control" id="formGroupExampleInput2" placeholder="Password" name="password" data-validation-help="Your Password here"><br>

      <button class="w3-btn w3-blue w3-hover-light-blue" name="submit">Log In</button><br>

    </fieldset>

  </form>

  <a href="forgetpassword"><button class="w3-btn w3-blue w3-hover-light-blue">Forget Password</button></a>

</div><br>

<!-- Login Portion end -->

<script>
$.validate({
    lang: 'en'
});
$.validate({
    modules : 'security',
    onModulesLoaded : function() {
    var optionalConfig = {
      fontSize: '12pt',
      padding: '6px',
      bad : 'Very Weak',
      weak : 'Weak',
      good : 'Good',
      strong : 'Strong'
    };
    $('input[name="pass_confirmation"]').displayPasswordStrength(optionalConfig);
  }
});
</script>
