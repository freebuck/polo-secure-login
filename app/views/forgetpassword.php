<!-- Forget Password Portion Start -->

<div class="main_tab_login">
  <h1>Forget Password</h1><br>
  <form action="" method="POST">
    <fieldset class="form-group">

      <label for="formGroupExampleInput">Username</label>
      <input name="username" type="text" class="form-control" id="formGroupExampleInput" placeholder="Username" data-validation-help="Input valid Username here"><br>

      <button class="w3-btn w3-blue w3-hover-light-blue" name="submit">Reset Password</button><br>
    </fieldset>
  </form>
</div>

<!-- Forget Password Portion End -->
<script>
$.validate({
    lang: 'en'
});
$.validate({
    modules : 'security',
    onModulesLoaded : function() {
    var optionalConfig = {
      fontSize: '12pt',
      padding: '6px',
      bad : 'Very Weak',
      weak : 'Weak',
      good : 'Good',
      strong : 'Strong'
    };
    $('input[name="pass_confirmation"]').displayPasswordStrength(optionalConfig);
  }
});
</script>