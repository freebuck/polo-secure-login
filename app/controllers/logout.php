<?php 
/**
* logout controllers
*/

class logout extends SystemController
{
	
	public function __construct()
	{
		session_start();
		session_unset(); 
		session_destroy(); 
		header("Location:http://www.freebuck.xyz/polo/"); //change here ex:http://yourwebsite.com/
	}
}

?>