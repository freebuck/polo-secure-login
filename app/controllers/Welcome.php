<?php
/**
* Welcome controllers
*/
class Welcome extends SystemController
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->view("Welcome");
	}
}
?>