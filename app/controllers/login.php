<?php
/**
* login controllers
*/

class login extends SystemController
{
	protected $timeCon;
	public function __construct()
	{
		parent::__construct();
		$this->load->view("login");
		if(isset($_POST['submit'])){
			$this->timeCon = new timeControll();

			$name = $this->con->real_escape_string($_POST['name']);
			$password = $this->con->real_escape_string($_POST['password']);
			$name = md5($name);
			$pass = md5($password);

			$check = "SELECT name FROM regis WHERE name = '$name' LIMIT 1";
			$result1 =$this->con->query($check);
			$result = mysqli_num_rows($result1);
			 if( $result == 0 ){
					echo "<div class='container warning_box'>";
					echo "<div class='alert alert-danger'>";
					echo "<a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
					echo "Username and password is not valid";
					echo "</div>";
					echo "</div>";
			   }//end if
			   	else{
			   		$sql ="SELECT * FROM regis WHERE name = '$name' LIMIT 1";
					$query = $this->con->query($sql);

				while($row = $query->fetch_array()){
				$id = $row['id'];
				$nam = $row['name'];
				$pas = $row['pass'];
				$wrong = $row['wrong'];
				$status = $row['status'];
				$time = $row['time'];
				$id_ac = $row['id_active'];
				$em = $row['email'];
				if ($nam == $name && $status == "block" && $time != 0) {
					//if status is block and time is not equal to zero
					echo "<div class='container warning_box'>";
					echo "<div class='alert alert-danger'>";
					echo "<a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
					echo "Your account is block for 10 minutes"; //alert message
				  	echo "</div>";
				  	echo "</div>";
				}
				elseif($nam == $name && $pas == $pass && $wrong <= 2 && $id_ac == "active"){
					//if everything is ok then it will be go next page
					$sql5 = "UPDATE regis SET time = 0 WHERE name = '$name'";
					$data5 = $this->con->query($sql5);
					$sql8 = "UPDATE regis SET wrong = 0 WHERE name = '$name'";
					$data8 = $this->con->query($sql8);
					if($data5 == true){
						$sql6 = "UPDATE regis SET status = 0 WHERE name = '$name'";
						$data6 = $this->con->query($sql6);
						ob_start();
						session_start();
						$_SESSION['web'] = "start";
						 //start a session
						header("Location:Welcome");   //go to welcome page
					}
				}

				elseif ($nam == $name && $pas != $pass && $wrong <= 2 ) {
					//if name will right and password will be wrong then this portion will be work
					$wrong = $wrong + 1;
					$sql2 = "UPDATE regis SET wrong = '$wrong' WHERE name = '$name'";
					$data2 = $this->con->query($sql2);
					if($data2 == true){
						echo "<div class='container warning_box'>";
						echo "<div class='alert alert-danger'>";
						echo "<a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
						echo "username or password is incorrect.";
					  	echo "</div>";
					  	echo "</div>";
					}
				}

				elseif ($nam == $name && $pas != $pass && $wrong == 3) {
					//if name is right and password is wrong and wrong limitation is proper then this portion will be work
					date_default_timezone_set('Asia/dacca');
					$wrong = 0;
					$sql2 = "UPDATE regis SET wrong = '$wrong' WHERE name = '$name'";
					$data2 = $this->con->query($sql2);
					if($data2 == true){
						$sql3 = "UPDATE regis SET status = 'block' WHERE name = '$name'";
						$data3 = $this->con->query($sql3);
						if($data3 == true){
							$time = date("Y-m-d H:i:s",time()+ 600);
							$sql4 = "UPDATE regis SET time = '$time' WHERE name = '$name'";
							$data4 = $this->con->query($sql4);
							include 'smtp/Send_Mail.php';
							$to = $em;
							$subject = "Account Danger Notice";
							$body = 'Hi, <br/> <br/> Someone is trying to access your account with wrong password.<br>If you are not please Contact with Site manager. Otherwise account will blocked for few moments.Thank you !!!!';
							Send_Mail($to, $subject, $body);
							echo "<div class='container warning_box'>";
							echo "<div class='alert alert-danger'>";
							echo "<a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
							echo "Don't Try again with your wrong password or username.";
						  	echo "</div>";
						  	echo "</div>";
							}
						}
					}
				}
			 }
		}//fun
	}
}
?>
