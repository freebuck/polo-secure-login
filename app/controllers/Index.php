<?php 
/**
* Index from controllers
*/

class Index extends SystemController
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function home(){
		$this->load->view("home");
	}
}
?>